import React from "react";
import { MenuContainer } from "../components";
import {UpdateUserFormContainer} from "../components/updateuserform/"
import {GetUserFormContainer } from "../components/getUser"
import "../screenCSS/profile.css"
import {Container, Col, Row } from "react-bootstrap";

export const ProfileScreen = () => (
  <div>
    <MenuContainer />
    <Container className="profileContainer">
      <Row>
        <Col className="colDesign">
          <GetUserFormContainer  />
        </Col>
        <Col className="colDesign" xs={7}>
          <UpdateUserFormContainer/>
        </Col>
      </Row>
    </Container>
  </div>
);
