import React from "react";
import { MenuContainer } from "../components";
import { LogOfUsersContainer } from "../components/logOfUsers"
import "../screenCSS/logOfUsers.css"

export const LogOfUsersScreen = () => (
  <div className="logScreen">
      <MenuContainer />
      <LogOfUsersContainer />
  </div>
)