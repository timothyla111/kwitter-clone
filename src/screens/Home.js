import React from "react";
import { LoginFormContainer} from "../components";
import { CreateUserFormContainer } from "../components/createuserform";
import '../screenCSS/home.css'
import {Container, Row, Col} from 'react-bootstrap'

export const HomeScreen = () => (
  <div className="homeApp">
    <Container className="contain">
    <Row className="decoyRow"></Row>
      <Row className="rowDesign">
        <Col className="colDesign1 backImage" xs={5}></Col>
        <Col className="collDesign">
          <LoginFormContainer />
          <CreateUserFormContainer /> 
        </Col>
      </Row>
    </Container>
  </div>
);
