import React from "react";
import { Link } from "react-router-dom";
import "../screenCSS/notFound.css"
import eggMan from '../Eggman.jpg'

const NotFound = ({ location }) => (
  <div className="notFoundContainer">
    <h2>Sorry, Not Sorry - Page Not Found for {location.pathname}</h2>
    <img src={eggMan} className="notFoundPic"/>
    <br />
    <br />
    <h4 className="thanks">
      A major shoutout to Suri, Howard P, Leanne B, and John W 
      for allocating their time, energy, and patience these past two weeks.
      We, Group M are beyond thankful for them guiding us 
      in the right direction to complete this project
      & to help us better understand React-Redux.
    </h4>
    <br />
    <h4>
      Special Shoutout to Vince for teaching us React-Redux this past month 
      and taking time out of his weekend to make
      a comprehensive scaffold of the kwitter assessment.
    </h4>
    <br />
    <h6 className="appreciate">
      I, Tim appreciate you Vince even though you called me Mr. Overachiever. I 
      am still smh brah! -_-
    </h6>
    <br />
    <Link to="/">Go Home</Link>
  </div>
);

export const NotFoundScreen = NotFound;
