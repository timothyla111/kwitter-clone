import React from "react";
import { MenuContainer } from "../components";
import { MessagesContainer } from "../components/messages";
import {MessagesFeedContainer} from "../components/messages";

export const MessageScreen = () => (
  <>
      <MenuContainer />
      <MessagesContainer />
      <MessagesFeedContainer />
  </>
)


