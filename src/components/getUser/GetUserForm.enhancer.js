import { connect } from "react-redux";
import {  getTheUser  } from "../../redux/actions/getUser"

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) => state.getUser


// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
  getTheUser 
};

export const enhancer = connect(mapStateToProps, mapDispatchToProps);
