import {enhancer} from "./GetUserForm.enhancer"
import {GetUserForm} from "./GetUserForm"

export const GetUserFormContainer = enhancer(GetUserForm)