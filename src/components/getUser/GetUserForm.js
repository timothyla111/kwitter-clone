import React, { useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import ProptTypes from "prop-types";
// import { Loader } from "../loader";
import "./GetUserForm.css";
import { Card, ListGroup, ListGroupItem, ProgressBar, Button} from "react-bootstrap";
import Avatar from "./images/AvatarCharacter.png"

export const GetUserForm = ({ getTheUser, displayName, createdAt, about, username }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    displayName: "",
    password: "",
    about: "",
  });
  
  const location = useParams()
  useEffect(() => {
    getTheUser(location.username)
  }, [])

  return (
    <div className="userInfoContainer">
      <Card style={{ width: '18rem' }} className="getUserInfo">
        <Card.Img variant="top" src={Avatar} className="userPic" />
        <Card.Body>
          <Card.Title className="cardInfo">{displayName}</Card.Title>
          <Card.Text className="cardInfo">
          Level 75 Gamer
          <ProgressBar animated now={75} />
          </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem className="userInfoList">@{username}</ListGroupItem>
          <ListGroupItem className="userInfoList">Updated At: {createdAt}</ListGroupItem>
        </ListGroup>
        <Card.Text className="cardInfo">
          {about}
        </Card.Text>
      </Card>
    </div>
  );
};

GetUserForm.propTypes = {
  getTheUser: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};
