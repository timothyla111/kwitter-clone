import { connect } from "react-redux";
import { messagesOutput} from "../../redux/actions/logOfMessages";

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) => state.logOfMessages

// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
  messagesOutput
  // {delete message action creator}
};

export const enhancerMessageFeed = connect(mapStateToProps, mapDispatchToProps);
