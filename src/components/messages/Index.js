import { enhancer } from "./Messages.enhancers";
import { Messages } from "./Messages";
import { enhancerMessageFeed} from "./MessageFeed.enhancer"
import {OutputLogOfMessages} from "./MessageFeed"

export const MessagesContainer = enhancer(Messages);
export const MessagesFeedContainer = enhancerMessageFeed(OutputLogOfMessages);
