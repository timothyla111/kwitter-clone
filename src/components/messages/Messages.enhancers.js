import { connect } from "react-redux";
import { createMessage } from "../../redux/actions/messages";

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) =>  state.logOfMessages

// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
  createMessage
};

export const enhancer = connect(mapStateToProps, mapDispatchToProps);
