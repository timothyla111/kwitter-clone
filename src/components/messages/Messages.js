import React, { useState } from "react";
import PropTypes from "prop-types";
// import { Loader } from "../loader";
// import MessageFeed from "./MessageFeed"
import "./Messages.css";
import { Button, Form} from "react-bootstrap";
import Slides from "./MesssagesSlides"


export const Messages = ({createMessage }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    text: "",
  });

  const handleMessage = (event) => {
    event.preventDefault();
    createMessage(state)
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <React.Fragment>
      <div className="alignHeader">
        <Slides />
        <Form className="formContain">
        <div class="form-group">
          <label for="exampleFormControlTextarea1"></label>
          <textarea 
            class="form-control" 
             id="exampleFormControlTextarea1"
             name="text" 
             onChange={handleChange} 
             rows="3"
             placeholder="What are you looking forward to get this week?"
            >
          </textarea>
        </div>
          <Button type="submit" class="btn btn-primary" onClick={handleMessage}>Submit Message</Button>
        </Form>
      </div>
    </React.Fragment>
  );
};

Messages.propTypes = {
  createMessage: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
};
