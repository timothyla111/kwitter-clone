import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Loader } from "../loader";
import "./Messages.css";
import { Media} from "react-bootstrap";
import gameAvatar from "./images/logo.png"
import "./Messages.css"


export const OutputLogOfMessages = ({loading, error, messagesOutput, messages }) => {

  useEffect(() => {
    messagesOutput()
  }, [])

    return (
      <React.Fragment>
        <ul className="listOfLogOfMessages">
            {
              messages.map(message => {
                  return (
                    <div key={message.id} className="differentMessages">
                    <Media className="mediaContain">
                      <img
                        width={64}
                        height={64}
                        className="mr-3"
                        src={gameAvatar}
                        alt="Game Avatar"
                      />
                    <Media.Body>
                      <h4>{message.username}</h4>
                      <h5>{message.text}</h5>
                      <h6>Created On: {message.createdAt}</h6>
                    </Media.Body>
                    {/* {button onclick to delete} */}
                    {/* {trigger action creator directly} */}
                    {/* {mapDispatchtoProps to } */}
                    {/* {Delete my message, not other users {use condition, compare if message.username (true), delete/don't delete}} */}
                    </Media>
                  </div>
                  )
              })
            }
            </ul>
        {loading && <Loader />}
        {error && <p style={{ color: "red" }}>{error.message}</p>}
      </React.Fragment>
    );  
};

OutputLogOfMessages.propTypes = {
  LogOfMessage: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
};





















          
    