import React from 'react'
import {Carousel} from "react-bootstrap"
import './Messages.css'
import Playstation from "./images/Ps5.jpg"
import VideoGame from "./images/Animal-Crossing.jpg"
import Accessory from "./images/GameChair.jpg"
import Clothes from "./images/Clothing.jpg"
import DragonBall from "./images/DragonBallZ.jpg"

function Slides() {
  return (
    <div className="containSlide">
      <Carousel className="carouselContainer">
        <Carousel.Item className="sliderContainer">
          <img
            className="slidersImg"
            src={Playstation}
            alt="PS5"
          />
          <Carousel.Caption className="slidersCaption">
            <h4 className="slidersText">Game System of The Week</h4>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="sliderContainer">
          <img
            className="slidersImg"
            src={VideoGame}
            alt="Video Game"
          />
          <Carousel.Caption className="slidersCaption">
            <h4 className="slidersText">Video Game of The Week</h4>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="sliderContainer">
          <img
            className="slidersImg"
            src={Accessory}
            alt="Game Chair"
          />
          <Carousel.Caption className="slidersCaption">
            <h4 className="slidersText">Accessory of The Week</h4>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="sliderContainer">
          <img
            className="slidersImg"
            src={Clothes}
            alt="Game Chair"
          />
          <Carousel.Caption className="slidersCaption">
            <h4 className="slidersText">Clothes of The Week</h4>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="sliderContainer">
          <img
            className="slidersImg"
            src={DragonBall}
            alt="Game Chair"
          />
          <Carousel.Caption className="slidersCaption">
            <h4 className="slidersText">Toy of The Week</h4>
          </Carousel.Caption>
        </Carousel.Item>

      </Carousel>
    </div>
  )
}

export default Slides
