import { connect } from "react-redux";
import { usersOutput} from "../../redux/actions/logOfUsers";

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) => state.logOfUsers

// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
  usersOutput,
};

export const enhancer = connect(mapStateToProps, mapDispatchToProps);
