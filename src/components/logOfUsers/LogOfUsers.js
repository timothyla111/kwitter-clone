import React, {useEffect } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import "./LogOfUsers.css";
import {Table} from 'react-bootstrap'

export const OutputLogOfUsers = ({usersOutput, loading, error, users }) => {

  useEffect(() => {
    usersOutput()
  }, [])

  return (
    <React.Fragment>
        <ul className="listOfLogOfUsers">
         {
          users.map(user => {
              return (
                <div key={user.username} className="differentUsers">
                <Table striped bordered hover variant="dark" className="tableSize">
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>Display Name</th>
                      <th>Joined On</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>@{user.username}</td>
                      <td>{user.displayName}</td>
                      <td>{user.createdAt}</td>
                    </tr>
                  </tbody>
                </Table>
              </div>
              )
          })
        }
        </ul>
      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};

OutputLogOfUsers.propTypes = {
  OutputLogOfUsers: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};


