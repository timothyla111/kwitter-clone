import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import "./CreateUserForm.css";
import {Form, Button} from 'react-bootstrap'
import {motion} from 'framer-motion'


export const CreateUserForm = ({ createUser, loading, error }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    username: "",
    displayName: "",
    password: "",
  });

  const handleCreateNewUser = (event) => {
    event.preventDefault();
    createUser(state);
    const { username, password, displayName } = state;
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <div className="formContainer">
     <motion.h3
      initial={{rotateZ: 180}}
      animate={{fontSize: 50, color: "blue", rotateZ: 0}}
      transition={{delay: 1.3}}
      >
        Enter The World of Games
      </motion.h3>

      <Form id="login-form" onSubmit={handleCreateNewUser}>
        <motion.div 
          initial={{x: "100vw"}} 
          animate={{x:0}}
          transition={{delay: 1.3}}
        >
        <Form.Group controlId="formBasicUser">
          <Form.Control 
            type="text" 
            name="username"
            value={state.username}
            autoFocus
            required
            onChange={handleChange}
            placeholder="Username" 
          />
        </Form.Group>

        <Form.Group controlId="formBasicDisplayName">
          <Form.Control
          type="text"
          name="displayName"
          value={state.displayName}
          required
          onChange={handleChange}
          placeholder="Display Name" 
        />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Control 
          type="password" 
          placeholder="Password" 
          name="password"
          value={state.password}
          required
          onChange={handleChange}
          />
        </Form.Group>
        </motion.div>

        <motion.div 
          initial={{rotateZ: 180}}
          animate={{rotateZ: 0}}
          transition={{delay: 1.3}}
        >
          <Button variant="primary"  type="submit" disabled={loading}>
            Register
          </Button>
        </motion.div>
        </Form>
        {loading && <Loader />}
        {error && <p style={{ color: "red" }}>{error.message}</p>} 
    </div>
  );
};

CreateUserForm.propTypes = {
  createUser: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};



