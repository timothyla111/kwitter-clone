import { enhancer } from "./CreateUserForm.enhancer";
import { CreateUserForm } from "./CreateUserForm";

export const CreateUserFormContainer = enhancer(CreateUserForm);
