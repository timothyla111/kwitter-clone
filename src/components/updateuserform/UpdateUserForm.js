import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import "./UpdateUserForm.css";
import { InputGroup, FormControl, Card, Button, Accordion} from "react-bootstrap";
import NintendoSwitch from "./images/Nintendo.jpg"

export const UpdateUserForm = ({ updateTheUser, displayName, about, username, deleteTheUser }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    username: username,
    displayName: displayName,
    about: about,
  });

  const handleUser = () => {
    // event.preventDefault();
    updateTheUser(state);
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  const deleteUser = () => {
    deleteTheUser(username)
  }

  return (
      <div className="container">
        <div className="updateUserContainer">
          <h1 className="updateHeader">Update Profile</h1>
            <InputGroup className="updateInput">
              <FormControl
                name="displayName"
                placeholder="Display Name"
                onChange={handleChange}
              />
            </InputGroup>

            <InputGroup className="updateInput">
              <InputGroup.Prepend>
                <InputGroup.Text>Gamer Status</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl 
                name="about"
                as="textarea" 
                onChange={handleChange}
              />
            </InputGroup>

            <Button 
              variant="primary" 
              className="updateInput button2"
              onClick={handleUser}
            >
              Update User
            </Button>

            <Button 
              variant="danger"
              className="updateInput button1" 
              onClick={deleteUser}
            >
              Delete User
            </Button>
          </div>

          <div className="imageContainer">
            <img src={NintendoSwitch} className="ninSwitch" />

            <Accordion>
              <Card className="motivationQuoteContainer">
                <Card.Header>
                  <Accordion.Toggle as={Button} variant="link" eventKey="0">
                    Daily Motivation
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="0">
                  <Card.Body className="motivationQuote">
                    "Consistently believe in yourself.
                    It's no coincidence that the word 
                    "inner" is in the word winner."
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
          </Accordion>
          </div>
      
      </div>
  );
};

// UpdateUserForm.propTypes = {
//   UpdateUserForm: ProptTypes.func.isRequired,
//   loading: ProptTypes.bool,
//   error: ProptTypes.string,
// };
