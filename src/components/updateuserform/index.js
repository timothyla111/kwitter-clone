import {enhancer} from "./UpdateUserForm.enhancer"
import {UpdateUserForm} from "./UpdateUserForm"

export const UpdateUserFormContainer = enhancer(UpdateUserForm)