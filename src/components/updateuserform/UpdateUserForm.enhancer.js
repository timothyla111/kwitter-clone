import { connect } from "react-redux";
import {  updateTheUser } from "../../redux/actions/updateUser"
import { deleteTheUser } from "../../redux/actions/deleteUser";

// https://react-redux.js.org/using-react-redux/connect-mapstate#connect-extracting-data-with-mapstatetoprops
const mapStateToProps = (state) => state.getUser

// https://react-redux.js.org/using-react-redux/connect-mapdispatch#connect-dispatching-actions-with-mapdispatchtoprops
const mapDispatchToProps = {
  updateTheUser, deleteTheUser
};

export const enhancer = connect(mapStateToProps, mapDispatchToProps);
