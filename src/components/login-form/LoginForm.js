import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import "./LoginForm.css";
import {InputGroup, Button, FormControl} from 'react-bootstrap'

export const LoginForm = ({ login, loading, error }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    username: "",
    password: "",
  });

  const handleLogin = (event) => {
    event.preventDefault();
    login(state);
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
      <div className="loginContainer">
          <form id="login-form" onSubmit={handleLogin}>
            <InputGroup className="test">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1" className="shadows">@</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                type="text"
                name="username"
                value={state.username}
                required
                onChange={handleChange}
                placeholder="username"
                className="loginBoxes shadows"
              />

              <FormControl
                type="password"
                name="password"
                value={state.password}
                required
                onChange={handleChange}
                placeholder="password"
                className="loginBoxes shadows"
              />

            <Button type="submit" disabled={loading} className="shadows">
              Login
            </Button>

            </InputGroup>
          </form>
          {loading && <Loader />}
          {error && <p style={{ color: "red" }}>{error.message}</p>}
        </div> 
  );
};

LoginForm.propTypes = {
  login: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};



