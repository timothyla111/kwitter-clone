// TODO: implement
import {
  USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILURE,
  CREATE_USER,
} from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  pictureLocation: null,
  about: "",
  password: "",
  displayName: "",
  username: "",
  loading: false,
  error: "",
};


export const createUser = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER:
      return {
        ...INITIAL_STATE,
        loading: true,
      };
    case CREATE_USER_SUCCESS:
      const {
        username,
        pictureLocation,
        displayName,
        about,
        password,
        token,
      } = action.payload;
      return {
        ...INITIAL_STATE,
        isAuthenticated: token,
        username,
        pictureLocation,
        displayName,
        about,
        password,
        loading: false,
      };
    case CREATE_USER_FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false,
      };
    case CREATE_USER:
      return {
        ...INITIAL_STATE,
      };
    default:
      return state;
  }
};

