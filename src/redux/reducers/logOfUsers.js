// TODO: implement
import { OUTPUT_USERS, OUTPUT_SUCCESS, OUTPUT_FAILURE } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
    users: [],
    loading: false,
    error: "",
};

export const logOfUsersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case OUTPUT_USERS:
      return {
        ...state,
        loading: true,
      };
    case OUTPUT_SUCCESS:
      return {
        ...state,
        users: action.payload.users,
        loading: false,
      };
    case OUTPUT_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
