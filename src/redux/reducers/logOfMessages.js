// TODO: implement
import { OUTPUT_MESSAGES, OUTPUT_MESSAGES_SUCCESS, OUTPUT_MESSAGES_FAILURE } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
    messages: [],
    loading: false,
    error: "",
};

export const logOfMessagesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case OUTPUT_MESSAGES:
      return {
        ...state,
        loading: true,
      };
    case OUTPUT_MESSAGES_SUCCESS:
      return {
        ...state,
        messages: action.payload.messages,
        loading: false,
      };
    case OUTPUT_MESSAGES_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
