import { GET_USER, GET_USER_SUCCESS, GET_USER_FAILURE} from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  username: "",
  password: "",
  displayName: "",
  about: "",
  createdAt: "",
  updatedAt: "",
  pictureLocation: "",
  loading: false,
  error: "",
};

export const getUserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...INITIAL_STATE,
        loading: true,
      };
    case GET_USER_SUCCESS:
      const { username, displayName, about, createdAt, updatedAt, pictureLocation, password } = action.payload.user;
      return {
        ...INITIAL_STATE,
        username,
        password,
        displayName,
        about,
        createdAt,
        updatedAt,
        pictureLocation,
        loading: false,
      };
    case GET_USER_FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
