import { UPDATE, UPDATE_SUCCESS, UPDATE_FAILURE} from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  isAuthenticated: false,
  username: "",
  loading: false,
  error: "",
};

export const updateUserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE:
      return {
        ...INITIAL_STATE,
        loading: true,
      };
    case UPDATE_SUCCESS:
      const { username, token } = action.payload;
      return {
        ...INITIAL_STATE,
        isAuthenticated: token,
        username,
        loading: false,
      };
    case UPDATE_FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
