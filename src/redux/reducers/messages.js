// TODO: implement
import { CREATE_MESSAGE, CREATE_MESSAGE_SUCCESS, CREATE_MESSAGE_FAILURE } from "../actions";
import { string } from "prop-types";

// INITIAL STATE
const INITIAL_STATE = {
    text: "",
    loading: false,
    error: "",
};

export const messageReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_MESSAGE:
      return {
        ...state,
        loading: true,
      };
    case CREATE_MESSAGE_SUCCESS:
      return {
        ...state,
        messages: action.payload.messages,
        loading: false,
      };
    case CREATE_MESSAGE_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
