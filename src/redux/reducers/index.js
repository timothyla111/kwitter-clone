import { combineReducers } from "redux";
import { authReducer } from "./auth";
import {logOfUsersReducer} from "./logOfUsers"
import {logOfMessagesReducer } from "./logOfMessages"
import {updateUserReducer} from "./updateUser"
import {getUserReducer} from "./getUser"
import {deleteUserReducer} from "./deleteUser"

export default combineReducers({ 
  auth: authReducer, 
  logOfUsers: logOfUsersReducer, 
  logOfMessages: logOfMessagesReducer,
  updateUser: updateUserReducer,
  getUser: getUserReducer,
  deleteUser: deleteUserReducer,
});
