import {DELETE_USER} from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  username: "",
  password: "",
  displayName: "",
  about: "",
  createdAt: "",
  updatedAt: "",
  pictureLocation: "",
  loading: false,
  error: "",
};

export const deleteUserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DELETE_USER:
      return {
        ...INITIAL_STATE,
      };
    default:
      return state;
  }
};
