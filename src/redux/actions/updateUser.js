import api from "../../utils/api";

export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";
export const UPDATE_USER_FAILURE = "UPDATE_USER_FAILURE";

/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/
export const updateTheUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: UPDATE_USER });
    const payload = await api.updateUser(credentials);
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({ type: UPDATE_USER_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: UPDATE_USER_FAILURE,
      payload: err.message,
    });
  }
};