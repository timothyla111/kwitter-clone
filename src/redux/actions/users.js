import api from "../../utils/api";

// AUTH CONSTANTS
export const USER = "USERS/USER";
export const CREATE_USER = "USERS/CREATE_USER";
export const CREATE_USER_FAILURE = "USERS/CREATE_USER_FAILURE";
export const CREATE_USER_SUCCESS = "USERS/CREATE_USER_SUCCESS";
//export const LOGOUT = "USERS/LOGOUT";

/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/
export const createUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: CREATE_USER });
    const payload = await api.createUser(credentials);
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({ type: CREATE_USER_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: CREATE_USER_FAILURE,
      payload: err.message,
    });
  }
};
