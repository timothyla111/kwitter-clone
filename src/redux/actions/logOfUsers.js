import api from "../../utils/api";

// AUTH CONSTANTS
export const OUTPUT_USERS = "OUTPUT_USERS";
export const OUTPUT_SUCCESS = "OUTPUT_SUCCESS";
export const OUTPUT_FAILURE = "OUTPUT_FAILURE";
// export const LOGOUT = "AUTH/LOGOUT";

export const usersOutput = () => async (dispatch, getState) => {
  try {
    dispatch({ type: OUTPUT_USERS });
    console.log()
    const payload = await api.getLogOfUsers();
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({ type: OUTPUT_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: OUTPUT_FAILURE,
      payload: err.OUTPUT,
    });
  }
};
