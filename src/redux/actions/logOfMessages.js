import api from "../../utils/api";

// AUTH CONSTANTS
export const OUTPUT_MESSAGES = "OUTPUT_MESSAGES";
export const OUTPUT_MESSAGES_SUCCESS = "OUTPUT_MESSAGES_SUCCESS";
export const OUTPUT_MESSAGES_FAILURE = "OUTPUT_MESSAGES_FAILURE";

export const messagesOutput = () => async (dispatch, getState) => {
  try {
    dispatch({ type: OUTPUT_MESSAGES });
    const payload = await api.getLogOfMessages();
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({ type: OUTPUT_MESSAGES_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: OUTPUT_MESSAGES_FAILURE,
      payload: err.OUTPUT,
    });
  }
};