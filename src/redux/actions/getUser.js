import api from "../../utils/api";

export const GET_USER = "GET_USER";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_FAILURE = "GET_USER_FAILURE";

/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/
export const getTheUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: GET_USER });
    const payload = await api.getUser(credentials);
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({ type: GET_USER_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: GET_USER_FAILURE,
      payload: err.message,
    });
  }
};
