import api from "../../utils/api";

export const DELETE_USER = "DELETE_USER";

/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/
export const deleteTheUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: DELETE_USER });
    const payload = await api.deleteUser(credentials);
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: DELETE_USER, payload });
  } catch (err) {
    dispatch({
      payload: err.message,
    });
  }
};