import api from "../../utils/api";
import {messagesOutput} from "./logOfMessages"

// AUTH CONSTANTS
export const MESSAGE = "MESSAGE";
export const MESSAGE_SUCCESS = "MESSAGE_SUCCESS";
export const MESSAGE_FAILURE = "MESSAGE_FAILURE";
// export const LOGOUT = "AUTH/LOGOUT";

export const createMessage = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: MESSAGE });
    console.log(credentials)
    const payload = await api.createMessage(credentials);
    // ℹ️ℹ️This is how you would debug the response to a requestℹ️ℹ️
    dispatch({ type: MESSAGE_SUCCESS, payload });
    dispatch(messagesOutput())
  } catch (err) {
    dispatch({
      type: MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};
